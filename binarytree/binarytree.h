#ifndef BINARYTREE_H
#define BINARYTREE_H

#include <stdio.h>

class Point{
public:
    float x,y;
};

class BinaryTree
{
    class Node
    {
    public:
        Node(int key, int depth);
        int key;
        int depth;
        int index;
        Node* left;
        Node* right;
    };

public:
    BinaryTree();
    void add(int key);
    void toGL();
    int depth();

    void recursivePrint(FILE *ofp, BinaryTree::Node *node);
    Point position(Node *node);

private:
    Node* _root;
    int _treeDepth;
    Node *search(int key);

};

#endif // BINARYTREE_H
