TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    main.cpp \
    binarytree.cpp \
    puregraph.cpp

OTHER_FILES += \
    Makefile

HEADERS += \
    binarytree.h \
    puregraph.h

