#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <time.h>
#include <vector>

#include "binarytree.h"

/**
 * @brief permutate list by shuffling it
 * @param list
 */
void shuffe(int *list){
    int n=100;
    for(int i=n-1; i>1; i--){
        int index = rand() % 100;
        int tmp = list[i];
        list[i] = list[index];
        list[index] = tmp;
    }
}


/**
 * @brief main
 *  Creates 100 random trees of which each depth is saved in a list.
 *  Each depth in the list will be compared to the values of x[0:100]
 *  The number of depths found that are below or equal to x are saved in another list.
 *  At the end the values of x and the values of the ylist are printed
 * @return
 */
int main(void)
{
    int list[100];
    int i=0;

    std::vector<int> depths;
    std::vector<int> xlist;
    std::vector<int> ylist;

    //init list
    for(i=0;i<100;i++){
        list[i]=i;
    }

    int below_depth = 0;
    srand (time(NULL));

    for(int j=0;j<100;j++){

        shuffe(list);

        BinaryTree tree = BinaryTree();
        for(i=0;i<100;i++){
            tree.add(list[i]);
        }
        tree.toGL();
        return 0;

        depths.push_back(tree.depth());
    }

    for(int x=0; x<100;x++){

        below_depth = 0;
        xlist.push_back(x);
        for(int i=0; i < depths.size(); i++){
            if( depths[i] <= x ){
                below_depth++;
            }
        }
        ylist.push_back(below_depth);
    }

     for(int i=0; i < xlist.size(); i++){
         std::cout << xlist[i] << std::endl;
     }

     for(int i=0; i < ylist.size(); i++){
         std::cout << ylist[i] << std::endl;
     }

    return 0;
}

