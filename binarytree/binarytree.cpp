#include "binarytree.h"
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <list>
#include <math.h>

/**
 * @brief BinaryTree::BinaryTree
 */
BinaryTree::BinaryTree()
{
    this->_root = NULL;
    this->_treeDepth = 0;
}

/**
 * @brief BinaryTree::search searches for the node which has the key \a key
 * @param key
 * @return the node with the value \a key or \a NULL if not found
 */
BinaryTree::Node* BinaryTree::search(int key)
{
    Node *current = this->_root;
    Node *location = NULL;
    while(current != NULL){
        location = current;

        if( current->key > key){
            current = current->left;
        }else{
            current = current->right;
        }
    }
    return location;
}

/**
 * @brief BinaryTree::add adds a new node with the value \a key to the tree
 *  The function will automatically determine the right location to insert the key and
 *  update the depth variable.
 * @param key
 */
void BinaryTree::add(int key)
{
    if( this->_root == NULL ){
        this->_root = new Node(key,0);
        this->_root->index = 1;
    }else{
        Node *node = this->search(key);
        if( node->key > key){

            int leftChildIndex = node->index * 2 - 1;
            node->left = new Node(key, node->depth+1);
            node->left->index = leftChildIndex;

            if( node->left->depth > this->_treeDepth ){
                this->_treeDepth = node->left->depth;
            }

        }else{

            int rightChildIndex = node->index * 2;
            node->right = new Node(key, node->depth+1);
            node->right->index = rightChildIndex;

            if( node->right->depth > this->_treeDepth ){
                this->_treeDepth = node->right->depth;
            }
        }
    }
}

/**
 * @brief BinaryTree::toGL creates a file named tree.gle to display the tree
 *
 */
void BinaryTree::toGL()
{
    FILE* ofp = fopen("tree.gle", "w");
    fprintf(ofp, "%s", "size 130 200\n");
    this->recursivePrint(ofp,this->_root);
    fclose(ofp);
}

/**
 * @brief BinaryTree::depth
 * @return the current depth of the tree
 */
int BinaryTree::depth()
{
    return this->_treeDepth;
}

/**
 * @brief BinaryTree::position this is used by the function \l toGL() to set the position of the nodes
 * @param node
 * @return the position x, y of \node
 */
Point BinaryTree::position(Node* node){

    int index = node->index;
    int depth = node->depth;

    int canvasWidth = 130;
    int canvasHeight = 190;
    float x = (1.0 * index * canvasWidth) / ( pow(2,depth) + 1);
    float y = canvasHeight - 1.0 * depth * canvasHeight / this->_treeDepth;
    Point pos = Point();
    pos.x = x;
    pos.y = y;
    return pos;
}

/**
 * @brief BinaryTree::recursivePrint
 * @param ofp
 * @param node
 */
void BinaryTree::recursivePrint(FILE* ofp ,BinaryTree::Node* node){

    Point pos = this->position(node);
    std::cout << pos.x << std::endl;
    fprintf(ofp, "%s%6.1f%6.1f\n", "amove ",pos.x, pos.y);
    fprintf(ofp, "%s\n", "circle 0.5 fill black");
    fprintf(ofp, "%s%d\n", "save v", node->key);

    if(node->left){
        this->recursivePrint(ofp, node->left);
        fprintf(ofp, "%s%d%s%d%s\n", "join v", node->key, ".cc - v", node->left->key, ".cc");
    }

    if(node->right){
        this->recursivePrint(ofp, node->right);
        fprintf(ofp, "%s%d%s%d%s\n", "join v", node->key, ".cc - v", node->right->key, ".cc");
    }
}

/**
 * @brief BinaryTree::Node::Node
 * @param key
 * @param depth
 */
BinaryTree::Node::Node(int key, int depth)
{
    this->key = key;
    this->depth = depth;
    this->left = NULL;
    this->right = NULL;
}
