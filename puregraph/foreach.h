#ifndef FOREACH_H
#define FOREACH_H

//taken from qt foreach: https://qt.gitorious.org/qt/qt/source/0726127285413829f58618b5b82fb3e2da0c3a74:src/corelib/global/qglobal.h#L2379-2383
template <typename T>
class ForeachContainer {
public:
    inline ForeachContainer(const T& t) : c(t), brk(0), i(c.begin()), e(c.end()) { }
    const T c;
    int brk;
    typename T::const_iterator i, e;
};

#define foreach(variable, container)                                \
for (ForeachContainer<__typeof__(container)> _container_(container); \
     !_container_.brk && _container_.i != _container_.e;              \
     __extension__  ({ ++_container_.brk; ++_container_.i; }))                       \
    for (variable = *_container_.i;; __extension__ ({--_container_.brk; break;}))


#endif // FOREACH_H
