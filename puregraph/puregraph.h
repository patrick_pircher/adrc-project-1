#ifndef PUREGRAPH_H
#define PUREGRAPH_H

#include <list>
#include <vector>

class Puregraph
{
    struct Node;
    typedef struct Edge{
        Puregraph::Node* v;
        Puregraph::Node* w;
    }Edge;

    typedef struct Node{
        int distance;
        bool visited;
        std::list<Edge*> edges;
        Node();
    }Node;

    std::vector<Node*> nodes;

private:
    int bfs(Node* root);

public:

    Puregraph(int n, float p);
    void generateRand(int n, float p);

    int diam();
    void show();
};

#endif // PUREGRAPH_H
