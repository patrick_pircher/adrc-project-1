#include "puregraph.h"
#include "random.h"

#include <iostream>
#include <vector>
#include "foreach.h"

int main(int argc, char** argv)
{
    randini();
    std::vector<int> xlist;
    std::vector<int> ylist;
    std::vector<int> diamList;

    for(int i=0; i<100; i++){

        Puregraph graph(100,0.01) ;
        int diam = graph.diam();
        diamList.push_back(diam);
    }

    for(int x=2; x<=100; x++){

        int below = 0;
        foreach(int diam, diamList)
        {
            if( diam <= x){
                below++;
            }
        }
        xlist.push_back(x);
        ylist.push_back(below);
    }

    foreach(int value, xlist){
        std::cout << value << std::endl;
    }

    foreach(int value, ylist){
        std::cout << value << std::endl;
    }
}



