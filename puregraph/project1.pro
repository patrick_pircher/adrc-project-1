TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt


OTHER_FILES += \
    Makefile

HEADERS += \
    puregraph.h \
    random.h \
    foreach.h

SOURCES += \
    puregraph.cpp \
    main.cpp \
    random.cpp

