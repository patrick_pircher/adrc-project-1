#include "puregraph.h"
#include "random.h"
#include <stdio.h>
#include "foreach.h"

/**
 * @brief Puregraph::Node::Node
 */
Puregraph::Node::Node()
{
    this->distance = 0;
    this->visited = false;
}

/**
 * @brief Puregraph::Puregraph
 * @param n
 * @param p
 */
Puregraph::Puregraph(int n, float p)
{
    for(int i=0; i<n; i++){
        Puregraph::Node* node = new Node();
        this->nodes.push_back(node);
    }
    this->generateRand(n,p);
}

/**
 * @brief Puregraph::generateRand
 * @param n
 * @param p
 */
void Puregraph::generateRand(int n,float p)
{
    for (int i = 0; i < n; i++){
        for (int j = 0; j <i; j++){
            if (randlcg(1) < p){
                Puregraph::Edge* edge = new Edge();
                edge->v = this->nodes[i];
                edge->w = this->nodes[j];
                this->nodes[i]->edges.push_front(edge);
                this->nodes[j]->edges.push_front(edge);
            }
        }
    }
}

/**
 * @brief Puregraph::diam
 * @return
 */
int Puregraph::diam()
{
    int maxDiam = 0;

    foreach(Puregraph::Node* node, this->nodes){

        int diam = this->bfs(node);
        if( diam > maxDiam ){
            maxDiam = diam;
        }

        for (int v = 0; v < this->nodes.size(); v++) {
            this->nodes.at(v)->distance = 0;
            this->nodes.at(v)->visited = false;
        }
    }
    return maxDiam;
}

/**
 * @brief Puregraph::bfs
 * @param root
 * @return
 */
int Puregraph::bfs(Node *root)
{
    std::list<Node*> queue;
    root->visited = true;
    queue.push_front(root);

    int maxDist = 0;

    while(!queue.empty()){

        Puregraph::Node* node = queue.front();
        queue.pop_front();
        node->visited = true;

        foreach(Puregraph::Edge* edge, node->edges){
            Puregraph::Node* adj = edge->v==node ? edge->w : edge->v;
            if( !adj->visited ){
                adj->distance = node->distance+1;
                if( adj->distance > maxDist ){
                    maxDist = adj->distance;
                }
                queue.push_back(adj);
            }
        }
    }
    return maxDist;
}

/**
 * @brief Puregraph::show
 */
void Puregraph::show()
{
    char* filename = "graph.gle";
    FILE* ofp = fopen(filename, "w");
    fprintf(ofp, "%s", "size 20 20\n");
    randini();
    for(int i=0; i<this->nodes.size(); i++) {
        this->nodes.at(i)->distance = i;
        fprintf(ofp, "%s%6.1f%6.1f\n", "amove ", rand_unif(2,13), rand_unif(3,21));
        fprintf(ofp, "%s\n", "circle 0.02 fill black");
        fprintf(ofp, "%s%d\n", "save v", i);
    }

    foreach(Puregraph::Node* node, this->nodes){

        foreach(Puregraph::Edge* edge, node->edges){

            fprintf(ofp, "%s%d%s%d%s\n", "join v", edge->v->distance, ".cc - v", edge->w->distance, ".cc");
        }
    }
    fclose(ofp);
}
