#ifndef GEOGRAPH_H
#define GEOGRAPH_H

#include <list>
#include <vector>

class Geograph
{
    struct Node;
    typedef struct Edge{
        Geograph::Node* v;
        Geograph::Node* w;
        float length;
    }Edge;

    typedef struct Node{
        float x,y;
    }Node;

    std::vector<Node*> nodes;
    std::vector<Edge*> edges;

private:
    int L;
    float a,b;
    float waxmanp(float d);

public:

    Geograph(int n, int L, float a, float b);
    void generateRand(int n);

    void show();
    int edgesBelow(int distance);
    float edgeLenghMean();
};

#endif // GEOGRAPH_H
