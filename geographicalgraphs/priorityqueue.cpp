#include "priorityqueue.h"
#include "stdlib.h"


#define exch(A, B)  {pitem t = A; A = B; B = t; }

template <typename T>
PriorityQueue<T>::PriorityQueue(int n)
{
    this->size = n ;
    this->items = 0;
    this->list = (pitem*) calloc(4 * 1024 ,sizeof(pitem));
}

template <typename T>
bool PriorityQueue<T>::empty()
{
    if ( this->items == 0 )
        return true;
    return false;
}

template <typename T>
void PriorityQueue<T>::clear()
{
    int i=0;
    while( i < this->items )
    {
        free(this->list[i].data);
        i++;
    }
    this->items = 0;
}

template <typename T>
bool PriorityQueue<T>::lessPri(pitem d1, pitem d2)
{
    return d1.priority >= d2.priority;
}

template <typename T>
void PriorityQueue<T>::fixUp()
{
    pitem *Heap = this->list;
    int Idx = this->items;

    while (Idx > 0  &&  lessPri(Heap[(Idx-1)/2], Heap[Idx]))
    {
        exch(Heap[Idx], Heap[(Idx-1)/2]);
        Idx = (Idx-1)/2;
    }
}

template <typename T>
void PriorityQueue<T>::fixDown(pitem *Heap, int Idx, int N)
{
    int Child; /* índice de um nó descendente  */
    while(2*Idx < N)
    { /* enquanto não chegar as folhas */
        Child =  2*Idx+1;
        /* selecciona o maior descendente.
        Nota: se índice Child  N-1, então sé há um descendente  */
        if (Child < N  &&  lessPri(Heap[Child], Heap[Child+1])) Child++;
        if (!lessPri(Heap[Idx], Heap[Child])) break; /*condição acervo  satisfeita
        */
        exch(Heap[Idx], Heap[Child]);
        /* continua a descer a árvore */
        Idx = Child;
    }
}


template <typename T>
void PriorityQueue<T>::add(T data, float priority )
{
    if( this->size < this->items + 1 )
    {
        if(this->size*2 > 4*1024*1024) /*4MB*/
        {
            this->list = (pitem*) realloc(this->list,(this->size + 4*1024*1024)*sizeof(pitem));
            this->size += 4*1024*1024;
        }else
        {
            this->list = (pitem*) realloc(this->list,2*this->size*sizeof(pitem));
            this->size *= 2;
        }
    }

    this->list[this->items].data = data;
    this->list[this->items].priority = priority;
    fixUp();
    this->items++;
}

template <typename T>
T PriorityQueue<T>::pop()
{
    exch(this->list[0], this->list[this->items-1]);

    fixDown(this->list, 0, this->items-2); /* ultimo elemento não considerado
    na reordenação */
    return this->list[--this->items].data;
}
