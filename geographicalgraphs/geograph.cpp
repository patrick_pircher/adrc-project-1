#include "geograph.h"
#include "math.h"
#include "random.h"
#include "priorityqueue.h"
#include "priorityqueue.cpp"
#include "foreach.h"
#include <stdio.h>
#include <iostream>


Geograph::Geograph(int n, int L, float a, float b)
    :L(L),a(a),b(b)
{
    for(int i=0; i<n; i++){
        Geograph::Node* node = new Node();
        node->x = rand_unif(0,this->L);
        node->y = rand_unif(0,this->L);
        this->nodes.push_back(node);
    }
    this->generateRand(n);
}

void Geograph::generateRand(int n)
{
    for (int i = 0; i < n; i++){
        for (int j = 0; j <i; j++){
            Geograph::Node* v = this->nodes[i];
            Geograph::Node* w = this->nodes[j];

            float d = sqrt(pow(v->x - w->x,2) + pow(v->y - w->y,2));

            if (randlcg(1) < this->waxmanp(d) ){

                Geograph::Edge* edge = new Edge();
                edge->v = v;
                edge->w = w;
                edge->length = d;
                this->edges.push_back(edge);
            }
        }
    }
    std::cout << "edges count: " << this->edges.size() << std::endl;
}

int Geograph::edgesBelow(int distance)
{
    int count = 0;
    foreach(Edge *edge, this->edges)
    {
        if(edge->length <= distance)
        {
            count++;
        }
    }
    return count;
}


float Geograph::edgeLenghMean(){

    float total_length = 0;
    foreach(Edge *edge, this->edges)
    {
        total_length += edge->length;
    }
    return total_length / this->edges.size();
}

float Geograph::waxmanp(float d)
{
    return a*exp(-b*d/L);
}





