#ifndef PRIORITYQUEUE_H
#define PRIORITYQUEUE_H

template <class T>
class PriorityQueue
{
    typedef struct pitem
    {
        T data;
        int priority;
    }pitem;
private:
    int items;
    double size;
    pitem *list;
    void fixDown(pitem *Heap, int Idx, int N);
    void fixUp();
    bool lessPri(pitem d1, pitem d2);
public:
    PriorityQueue<T>(int n);
    void add(T data, float priority);
    T pop();
    void clear();
    bool empty();
    int isEmpty();
};

#endif // PRIORITYQUEUE_H
