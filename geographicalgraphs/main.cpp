#include "geograph.h"
#include "random.h"

#include <iostream>
#include <vector>
#include "foreach.h"
#include <math.h>

int main(int argc,char** argv)
{
    randini();
    std::vector<int> xlist;
    std::vector<int> ylist;
    std::vector<int> diamList;


    int L = 200;
    float a = 1;
    float b = 4.71;
    Geograph graph(100,L,a,b) ;
    std::cout << graph.edgeLenghMean() << std::endl;


    float x_max = sqrt(2)*L;

    for(int x=2; x<=x_max; x++){

        int nEdges = graph.edgesBelow(x);
        xlist.push_back(x);
        ylist.push_back(nEdges);
    }

    foreach(int value, xlist){
        std::cout << value << std::endl;
    }

    foreach(int value, ylist){
        std::cout << value << std::endl;
    }
}
