import qbs 1.0

Application {

    name: 'geograph'

    Group {
        name: "Sources"
        files: [
            "*.cpp"
        ]
    }

    Group {
        name: "Headers"
        files: [
            "*.h"
        ]
    }
    Depends { name:"cpp" }
    cpp.linkerFlags: [
        "-static",
    ]

}


