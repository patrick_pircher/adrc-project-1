TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt


OTHER_FILES += \
    Makefile

HEADERS += \
    geograph.h \
    random.h \
    priorityqueue.h \
    foreach.h

SOURCES += \
    geograph.cpp \
    main.cpp \
    random.cpp \
    priorityqueue.cpp
